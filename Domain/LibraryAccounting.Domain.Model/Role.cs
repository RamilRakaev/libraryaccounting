﻿using System.Collections.Generic;

namespace LibraryAccounting.Domain.Model
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
